package webapp;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ImageServlet extends HttpServlet {
    public static List<String> getIdList() {
        return idList;
    }

    private static List<String> idList = new ArrayList<>();
    private List<Blob> imageList = new ArrayList<>();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {
        ServletOutputStream out = response.getOutputStream();
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/newDB", "root", "user");

        if (conn != null) {
            try {
                String wishId = request.getParameter("wishId"); /*Уникальный идентификатор изображения*/

                HttpSession session = request.getSession();
                String username = (String) session.getAttribute("username");

                String sql = "SELECT pic FROM Wishes WHERE wishId='" + wishId + "' AND username='" + username + "'"; //!!!!
                //String count = "SELECT COUNT(*) FROM Wishes WHERE wishId='" + wishId + "' AND username='" + username + "'";
                java.sql.Statement stmt = conn.createStatement();
                java.sql.ResultSet rs = stmt.executeQuery(sql);
                //java.sql.ResultSet crs = stmt.executeQuery(count);
                //System.out.println(count);
                while (rs.next()) {
                    Blob image = rs.getBlob("pic");/*Изображение*/
                    imageList.add(image);
                    InputStream in = image.getBinaryStream();
                    int length;
                    int bufferSize = 1024;
                    byte[] buffer = new byte[bufferSize];
                    while ((length = in.read(buffer)) != -1) {
                        out.write(buffer, 0, length);
                    }
                    in.close();
                    out.flush();
                }
                rs.close();
                stmt.close();
                conn.close();
            }
            catch (SQLException ex) {
                Logger.getLogger(ImageServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        }
        catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        }
        catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }
}
