package webapp;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginServlet extends HttpServlet {

    public static boolean checkUser(String username, String pass)
    {
        boolean st =false;
        try{

            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/newDB", "root", "user");

            PreparedStatement ps =con.prepareStatement
                    ("select * from Student where username=? and pass=?");
            ps.setString(1, username);
            ps.setString(2, pass);
            ResultSet rs =ps.executeQuery();
            st = rs.next();

        }catch(Exception e)
        {
            e.printStackTrace();
        }
        return st;
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        String username = request.getParameter("username");
        String pass = request.getParameter("pass");

        if (checkUser(username, pass)) {
            RequestDispatcher rs = request.getRequestDispatcher("index.jsp");
            HttpSession session = request.getSession();
            session.setAttribute("username", username);
            System.out.println(username);
            rs.forward(request, response);
            
        } else {

            RequestDispatcher rs = request.getRequestDispatcher("RegisterServlet.jsp");
            //response.sendRedirect("signIn.jsp");
            out.println("Username or Password incorrect");
            rs.include(request, response);
        }
    }


}
