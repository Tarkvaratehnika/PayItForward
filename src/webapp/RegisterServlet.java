package webapp;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;


public class RegisterServlet extends HttpServlet {
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        String n = request.getParameter("name");
        String e = request.getParameter("email");
        String u = request.getParameter("username");
        String p = request.getParameter("pass");


        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/newDB", "root", "user");

            PreparedStatement ps = con
                    .prepareStatement("insert into Student values(DEFAULT ,?,?,?,?)");

            ps.setString(1, n);
            ps.setString(2, e);
            ps.setString(3, u);
            ps.setString(4, p);


            int i = ps.executeUpdate();
            if (i > 0) {
                response.sendRedirect("index.jsp");
            }


        } catch (Exception e2) {
            System.out.println(e2);
        }

        out.close();
    }

}