<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>New Wish</title>

    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row centered-form">
        <div class="col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Add your new wish</h3>
                </div>
                <div class="panel-body">
                    <form role="form" method="post" action="/saveImage" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <input type="text" name="wishName" id="wishName" class="form-control input-sm" placeholder="Wish Name">
                                </div>
                            </div>
                        </div>
                        <div name="photo">
                            <input type="file"  name="pic" accept="image/*">
                        </div>
                        <br>
                        <div class="form-group">
                            <label for="description">Description:</label>
                            <textarea class="form-control" rows="5" id="description" name="description"></textarea>
                        </div>

                        <input type="submit" value="Add"  style="background-color: #0d71bb; border-color: #0d71bb" class="btn btn-info btn-block">

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
    body{
        background-color: #ffffff;
    }
    .centered-form{
        margin-top: 60px;
    }

    .centered-form .panel{
        background: rgba(255, 255, 255, 0.8);
        box-shadow: rgba(0, 0, 0, 0.3) 20px 20px 20px;
    }
</style>

</body>
</html>
