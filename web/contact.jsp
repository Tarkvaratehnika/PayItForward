<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1, maximum-scale=2, user-scalable=no" name="viewport">
    <meta content="Semantic-UI-Forest, collection of design, themes and templates for Semantic-UI." name="description">
    <meta content="Semantic-UI, Theme, Design, Template" name="keywords">
    <meta content="PPType" name="author">
    <meta content="#ffffff" name="theme-color">
    <title>Contact</title>
    <link href="css/semantic.min.css" rel="stylesheet" type="text/css">
    <link href="../../static/stylesheets/default.css" rel="stylesheet" type="text/css">
    <link href="../../static/stylesheets/pandoc-code-highlight.css" rel="stylesheet" type="text/css">
    <script src="../../static/dist/jquery/jquery.min.js"></script>
</head>
<body>
<div class="ui inverted vertical center aligned segment" id="whole">
    <div class="ui container">
        <div class="ui black inverted borderless huge fluid menu">
            <a class="active item" href="index.jsp">Pay It Forward</a>

            <%
                String username = (String) session.getAttribute("username");
                if (username == null) { %>

            <a class="header item" href="signIn.jsp" style="margin-right: 0px">Sign In</a>
            <% } else { %>

            <a href="Wishes.jsp" class="item">My Wishes</a>
            <a href="Logout" class="item">Log Out</a>

            <% } %>

        </div>
    </div>
    <div class="ui grid">
        <div class="three column row" id="slide">
            <div class="active column" data-tab="1">
                <div class="ui inverted vertical center aligned segment" >
                    <div class="ui active text container" style="color:grey">
                        <h1 class="ui inverted header" style="color:grey">
                            Our contacts
                        </h1>
                        <div>
                            <ul class="list-unstyled">
                                <li>
                                    <div class="icon"><i class="fa fa-map-marker" style="font-size: 30px"></i></div>
                                    <a class="st" href="https://goo.gl/maps/37dP6kSoLBv" target="_blank"> Akadeemia tee 15a, 12616 Tallinn</a>
                                </li>

                                <li>
                                    <div class="icon"><i class="fa fa-phone"></i></div>
                                    <a class="st" href="tel:0000">+372 6205552</a>
                                </li>

                                <li>
                                    <div class="icon"><i class="fa fa-envelope"></i></div>
                                    <a class="st" href="mailto:payitforward@ttu.ee">payitforward@ttu.ee</a>
                                </li>
                                <li>
                                    <div class="icon"><i class="fa fa-info"></i></div>
                                    <span class="st">PayItForward is an easy to use web-based application that will help you collect,
                                    organize and keep track of the things you want. It also makes it easy to share
                                    those things with friends and family.</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>





<div class="ui footer container">
    <div class="ui divider"></div>
    <div class="ui two column grid">
        <div class="column">
            <p>
                © 2018 Pay It Forward, Inc.
            </p>
        </div>
        <div class="right floated two wide column">
            <a href="Wishes.html">Back to top</a>
        </div>
    </div>
</div>
<script src="css/semantic.min.js"></script>
<style type="text/css">
    body .ui.inverted.segment {
        background: transparent;
    }

    .icon {
        color: black;
    }
    .fa {
        font-size: 25px;
    }

    .st {
        color: #696969;
        margin-top: 20px;
        margin-bottom: 5px;
        font-weight: 200;
        font-size: 19px;

    }

    #whole.segment {
        height: 500px;
        background-image: url("images/header.jpg");
    }

    .ui.borderless.inverted.menu {
        background-color: #2f2f2f;
        border-radius: 4px;
    }
    .ui.borderless.inverted.menu .active.item {
        background-color: black;
    }

    p code {
        background-color: white;
        border-radius: 4px;
    }

    #slide.row {
        width: 300vw !important;
        overflow: hidden;
        position: absolute;
        margin-top: 5em;
    }
    #slide.row .column {
        padding: 0 !important;
    }
    #slide.row .column h1.header {
        font-size: 2.5em;
    }
    #slide.row .column p {
        font-size: 1.4em;
        line-height: 1.4;
    }

    #control.grid {
        position: relative;
        top: 7em;
    }
    #control.grid .ui.button {
        box-shadow: none;
    }
    #control.grid .ui.button:hover {
        background: transparent !important;
    }
    #control.grid .ui.button .icon {
        color: #bfbfbf;
    }

    #bubble.row {
        position: absolute;
        bottom: 0;
    }

    .ui.grid .ui.items .item .content {
        align-self: center;
    }
    .ui.grid .ui.items .item .content h1 {
        font-size: 3.5em;
    }
    .ui.grid .ui.items .item .content h1 span.disabled {
        color: grey;
    }
    .ui.grid .ui.items .item .content p {
        font-size: 1.5em;
        line-height: 1.5;
    }

    .ui.footer.container {
        font-size: 1.2em;
    }
</style>


</body>
</html>