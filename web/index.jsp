<%@ page import="java.util.ArrayList" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.io.InputStream" %>
<%@ page import="java.io.OutputStream" %>
<html><head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1, maximum-scale=2, user-scalable=no" name="viewport">
    <meta content="Semantic-UI-Forest, collection of design, themes and templates for Semantic-UI." name="description">
    <meta content="Semantic-UI, Theme, Design, Template" name="keywords">
    <meta content="PPType" name="author">
    <meta content="#ffffff" name="theme-color">
    <title>Pay It Forward</title>
    <link href="css/semantic.min.css" rel="stylesheet" type="text/css">
    <link href="../../static/stylesheets/default.css" rel="stylesheet" type="text/css">
    <link href="../../static/stylesheets/pandoc-code-highlight.css" rel="stylesheet" type="text/css">
    <script src="../../static/dist/jquery/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

</head>
<body>
<div class="ui inverted vertical center aligned segment" id="whole">
    <div class="ui container">
        <div class="ui black inverted borderless huge fluid menu">
            <a class="active item" href="index.jsp">Pay It Forward</a>
            <a class="item" href="contact.jsp">Contact</a>

            <%
                String username = (String) session.getAttribute("username");
                if (username == null) { %>

            <a class="header item" href="signIn.jsp" style="margin-right: 0px">Sign In</a>
            <% } else { %>
            <a class="item" href="Wishes.jsp">My Wishes</a>
            <a href="Logout" class="item">Log Out</a>

            <% } %>
            <div class="container">
                <div class="row">
                    <div id="custom-search-input">
                        <form class="input-group col-md-12" method="post" action="search.jsp">
                            <input type="text" class="search-query form-control" id="search" name="search" placeholder="enter username" required="required"/>
                            <input class="btn btn-secondary" type="submit" value="Search"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ui grid">
        <div class="three column row" id="slide">
            <div class="active column" data-tab="1">
                <div class="ui inverted vertical center aligned segment" >
                    <div class="ui active text container" style="color:grey">
                        <h1 class="ui inverted header" style="color:grey">
                            Pay It Forward
                        </h1>

                        <%
                            if (username == null) {
                        %>

                        <div>
                            <a class="ui huge primary button" href="register.jsp">Sign up today</a>
                        </div>
                        <%}%>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="ui section hidden divider"></div>
<div class="ui three column center aligned stackable grid container">

    <%

        try
        {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/newDB", "root", "user");
            String query = "SELECT * FROM Wishes";
            Statement stmt=conn.createStatement();
            ResultSet rs=stmt.executeQuery(query);
            while(rs.next())
            {

    %>

    <div class="column">
        <img class="ui centered small circular image" src="/images?wishId=<%=rs.getInt("wishId") %>" border="0" width="50" height="50" />

        <h2 class="ui header">
            <%=rs.getString("wishName") %>
        </h2>
        <h4 class="ui header">
            Posted by: <%=rs.getString("username") %> <br>
        </h4>

        <p>Description: <%=rs.getString("description")%></p>
        <%
            if (username != null) { %>
        <form method="POST">
            <button type="submit">
                <input type="hidden" name="<%=rs.getInt("wishId")%>" value="1">
                <input type="image" src="images/thumbsup.png" aria-hidden="true" style="width: 20px; height: 20px;"></input>
            </button>
        </form><br>
        <%if (request.getParameter(String.valueOf(rs.getInt("wishId"))) != null) {
            String query2 = "UPDATE wishes SET `like` = ? WHERE `wishId` = ?";
            PreparedStatement ps = conn.prepareStatement(query2);
            int score = rs.getInt("like") + 1;
            ps.setInt(1, score);

            ps.setInt(2, rs.getInt("wishId"));
            ps.executeUpdate();
            ps.close();

            }
        }
        %>

        <br>
    </div>

    <%
            }
            rs.close();
            stmt.close();
            conn.close();
        }
        catch(Exception e)
        {
            out.println("Exception:" + e.getMessage());
            e.printStackTrace();
        }
    %>
</div>

<div class="ui footer container">
    <div class="ui divider"></div>
    <div class="ui two column grid">
        <div class="column">
            <p>
                © 2018 Pay It Forward, Inc.
            </p>
        </div>
        <div class="right floated two wide column">
            <a href="index.jsp#">Back to top</a>
        </div>
    </div>
</div>


<script src="css/semantic.min.js"></script>
<style type="text/css">
    body .ui.inverted.segment {
        background: transparent;
    }

    #whole.segment {
        height: 500px;
        background-image: url("images/header.jpg");
    }

    .ui.borderless.inverted.menu {
        background-color: #2f2f2f;
        border-radius: 4px;
    }
    .ui.borderless.inverted.menu .active.item {
        background-color: black;
    }

    p code {
        background-color: white;
        border-radius: 4px;
    }

    #slide.row {
        width: 300vw !important;
        overflow: hidden;
        position: absolute;
        margin-top: 5em;
    }
    #slide.row .column {
        padding: 0 !important;
    }
    #slide.row .column h1.header {
        font-size: 2.5em;
    }
    #slide.row .column p {
        font-size: 1.4em;
        line-height: 1.4;
    }

    #control.grid {
        position: relative;
        top: 7em;
    }
    #control.grid .ui.button {
        box-shadow: none;
    }
    #control.grid .ui.button:hover {
        background: transparent !important;
    }
    #control.grid .ui.button .icon {
        color: #bfbfbf;
    }

    #bubble.row {
        position: absolute;
        bottom: 0;
    }

    .ui.grid .ui.items .item .content {
        align-self: center;
    }
    .ui.grid .ui.items .item .content h1 {
        font-size: 3.5em;
    }
    .ui.grid .ui.items .item .content h1 span.disabled {
        color: grey;
    }
    .ui.grid .ui.items .item .content p {
        font-size: 1.5em;
        line-height: 1.5;
    }

    .ui.footer.container {
        font-size: 1.2em;
    }
    #custom-search-input {
        margin:0;
        margin-top: 10px;
        padding: 0;
    }

    #custom-search-input .search-query {
        padding-right: 3px;
        padding-right: 4px \9;
        padding-left: 3px;
        padding-left: 4px \9;
        /* IE7-8 doesn't have border-radius, so don't indent the padding */

        margin-bottom: 0;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
    }

    #custom-search-input button {
        border: 0;
        background: none;
        /** belows styles are working good */
        padding: 2px 5px;
        margin-top: 2px;
        position: relative;
        left: -28px;
        /* IE7-8 doesn't have border-radius, so don't indent the padding */
        margin-bottom: 0;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        color:#D9230F;
    }

    .search-query:focus + button {
        z-index: 3;
    }

</style>



</body></html>